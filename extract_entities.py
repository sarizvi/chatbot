import datetime
import json
import re

import pymysql
from dateparser.search import search_dates
from flask import Flask, jsonify, request, abort

from config import *

app = Flask(__name__)


week_days = {'monday': 0, 'tuesday': 1, 'wednesday': 2, 'thursday': 3, 'friday': 4, 'saturday': 5, 'sunday': 6}
timings = {'early morning': '00:00 to 06:00', 'morning': '06:00 to 12:00', 'noon': '12:00',
           'afternoon': '12:00 to 18:00',
           'evening': '18:00 to 20:00', 'eve': '18:00 to 20:00', 'night': '20:00 to 24:00'}


def next_weekday(d, weekday):
    days_ahead = week_days[weekday] - d.weekday()
    if days_ahead <= 0:
        days_ahead += 7
    return d + datetime.timedelta(days_ahead)


def previous_weekday(d, weekday):
    days_ahead = week_days[weekday] - d.weekday()
    if days_ahead <= 0:
        days_ahead += 7
    return d - datetime.timedelta(days_ahead)


@app.route('/parse_query', methods = ['GET'])
def extract_cities():
    with app.app_context():
        output_json = {}
        questions = []
        input = request.args.get('q')
        if not input:
            abort(404, "Missing query!")
        conn = pymysql.connect(host = db_host, user = db_user, passwd = db_pass, port = 3306, db = db_name)
        curr = conn.cursor()
        sql = "SELECT city FROM places"
        curr.execute(sql)
        result = curr.fetchall()
        curr.close()
        conn.close()
        cities = []
        for city in result:
            cities.append(city[0].lower())
        input_tokens = input.split()
        cities_map = []
        for index, word in enumerate(input_tokens):
            if word.lower() in cities:
                cities_map.append({'index': index, 'name': word})
            else:
                continue

        for city in cities_map:
            if input_tokens[city['index'] - 1] == 'from':
                output_json['from'] = city['name']
            elif input_tokens[city['index'] - 1] == 'to':
                output_json['to'] = city['name']
        if input.find('return') == -1:
            output_json['is_return_query'] = False
        else:
            output_json['is_return_query'] = True
            if len(cities_map) == 2:
                if input.find('from') != -1:
                    if input.find('return') < input.find('from'):
                        temp = output_json['to']
                        output_json['to'] = output_json['from']
                        output_json['from'] = temp
                else:
                    output_json['from'] = None
            elif len(cities_map) == 1:
                if input.find('from') != -1:
                    if input.find('return') < input.find('from'):
                        output_json['to'] = output_json['from']
                        output_json['from'] = None
                else:
                    output_json['from'] = None
            else:
                output_json['from'] = None
                output_json['to'] = None
        if output_json['from'] is None:
            questions.append('what is the source city?')
        if output_json['to'] is None:
            questions.append('what is the destination city?')
        final_output_json = extract_dates(input)
        final_output_json['from'] = output_json['from']
        final_output_json['to'] = output_json['to']
        for question in questions:
            final_output_json['questions'].append(question)
        return jsonify(final_output_json)


def extract_time(input, initial_return_keyword_index):
    temp_json = {}
    questions = []
    input = input.lower()
    time_list = list(timings.keys())
    timings_present_in_query = []
    for time in time_list:
        if input.find(time) == -1:
            continue
        else:
            timings_present_in_query.append(time)
    if len(timings_present_in_query) == 0:
        questions.append('what time would be suitable for your onward flight?')
        questions.append('what time would be suitable for your return flight?')
    if len(timings_present_in_query) == 1:
        if initial_return_keyword_index != -1:
            if initial_return_keyword_index > input.find(timings_present_in_query[0]):
                temp_json['onward_time'] = timings[timings_present_in_query[0]]
                temp_json['return_time'] = ""
                questions.append('What time would be suitable for return flight?')
            else:
                temp_json['return_time'] = timings[timings_present_in_query[0]]
                temp_json['onward_time'] = ""
                questions.append('What time would be suitable for onward flight?')
        else:
            temp_json['onward_time'] = timings[timings_present_in_query[0]]
    elif len(timings_present_in_query) >= 2:
        if initial_return_keyword_index > input.find(timings_present_in_query[0]):
            temp_json['onward_time'] = timings[timings_present_in_query[0]]
            temp_json['return_time'] = timings[timings_present_in_query[1]]
        else:
            temp_json['return_time'] = timings[timings_present_in_query[0]]
            temp_json['onward_time'] = timings[timings_present_in_query[1]]
    temp_json['questions'] = questions
    return temp_json


def extract_dates(input):
    questions = []
    temp_json = {}
    conn = pymysql.connect(host = db_host, user = db_user, passwd = db_pass, port = 3306, db = db_name)
    curr = conn.cursor()
    sql = "SELECT name,type,date FROM events"
    curr.execute(sql)
    result = curr.fetchall()
    curr.close()
    conn.close()
    for event in result:
        name = event[0]
        if input.lower().find(name.lower()) != -1 and event[1] != 'personal':
            input = re.sub(name.lower(), event[2], input.lower())
        if input.lower().find(name.lower()) != -1 and event[1] == 'personal':
            questions.append('what is date for ' + event[0] + '?')
    input = input.replace(" to ", "")
    if input.find('return') == -1:
        temp_json['return_date'] = None
        temp_json['return_time'] = None
    dp = search_dates(input, settings = {'PREFER_DATES_FROM': 'future', 'TIMEZONE': 'UTC+5:30'})
    dates = []
    for date in dp:
        text = date[0].replace("on ", "").replace("and", "").strip()
        dates.append([text, date[1]])
    event_name = ""
    after_index = -1
    before_index = -1
    if len(dates) == 3:
        input_tokens = input.split()
        for index, word in enumerate(input_tokens):
            if word.lower() == 'after':
                after_index = index
                event_name = input_tokens[index + 1]
                break
            elif word.lower() == 'before':
                before_index = index
                event_name = input_tokens[index + 1]
                break
        if after_index != -1:
            initial_onward_index = input.find(dates[0][0])
            initial_return_keyword_index = input.find('return')
            temp_time = extract_time(input, initial_return_keyword_index)
            if initial_return_keyword_index < initial_onward_index:
                final_onward_date = dates[1]
                final_return_date = dates[0]
                onward_day = next_weekday(dates[2][1].date(), final_onward_date[0].lower())
                # onward_day = final_onward_date[1].date()
                return_day = next_weekday(onward_day, final_return_date[0].lower())
                temp_json['return_date'] = str(return_day)
                temp_json['onward_date'] = str(onward_day)
            else:
                # final_onward_date = dates[0]
                # final_return_date = dates[1]
                final_onward_date = next_weekday(dates[2][1].date(), dates[0][0].lower())
                final_return_date = next_weekday(final_onward_date, dates[1][0].lower())
                temp_json['return_date'] = str(final_return_date)
                temp_json['onward_date'] = str(final_onward_date)
        if before_index != -1:
            initial_onward_index = input.find(dates[0][0])
            initial_return_keyword_index = input.find('return')
            temp_time = extract_time(input, initial_return_keyword_index)
            if initial_return_keyword_index < initial_onward_index:
                final_onward_date = dates[1]
                final_return_date = dates[0]
                onward_day = previous_weekday(dates[2][1].date(), final_return_date[0].lower())
                # onward_day = final_onward_date[1].date()
                return_day = next_weekday(onward_day, final_return_date[0].lower())
                temp_json['return_date'] = str(return_day)
                temp_json['onward_date'] = str(onward_day)
            else:
                # final_onward_date = dates[0]
                # final_return_date = dates[1]
                final_onward_date = next_weekday(dates[2][1].date(), dates[0][0].lower())
                final_return_date = next_weekday(final_onward_date, dates[1][0].lower())
                temp_json['return_date'] = str(final_return_date)
                temp_json['onward_date'] = str(final_onward_date)
    elif len(dates) == 2:
        initial_onward_index = input.find(dates[0][0])
        initial_return_keyword_index = input.find('return')
        temp_time = extract_time(input, initial_return_keyword_index)
        if initial_return_keyword_index < initial_onward_index:
            final_onward_date = dates[1]
            final_return_date = dates[0]
            onward_day = final_onward_date[1].date()
            return_day = next_weekday(onward_day, final_return_date[0].lower())
            temp_json['return_date'] = str(return_day)
            temp_json['onward_date'] = str(onward_day)
        else:
            final_onward_date = dates[0]
            final_return_date = dates[1]
            temp_json['return_date'] = str(final_return_date[1].date())
            temp_json['onward_date'] = str(final_onward_date[1].date())
    elif len(dates) == 1:
        initial_onward_index = input.find(dates[0][0])
        initial_return_keyword_index = input.find('return')
        temp_time = extract_time(input, initial_return_keyword_index)
        if initial_return_keyword_index == -1:
            temp_json['onward_date'] = str(dates[0][1].date())
            temp_json['return_date'] = ""
        elif initial_return_keyword_index < initial_onward_index:
            temp_json['return_date'] = str(dates[0][1].date())
            temp_json['onward_date'] = ""
            questions.append('When do u want to go?')

    temp_json['questions'] = questions
    for question in temp_time['questions']:
        temp_json['questions'].append(question)
    if temp_time:
        if 'onward_time' in temp_time:
            temp_json['onward_time'] = temp_time['onward_time']
        if 'return_time' in temp_time:
            temp_json['return_time'] = temp_time['return_time']
    return temp_json


if __name__ == '__main__':
    app.run(host = '0.0.0.0', port = 8500, debug = True)