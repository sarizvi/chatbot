﻿# Aertrip
To design, develop and deploy an application to parse flight search query for a user using python without NLP/AI engines.

## Installation

```bash
1. Clone the git repository from gitlab.com.
2. Open terminal and go to the project directory.
3. Create a virtual environment using command: virtualenv -p python3 environment_name.
4. Activate the virtual environment using command: source environment_name/bin/activate
5. Install all the dependencies using command: pip install -r requirements.txt.
6. Take the mysql dump and import it onto your local db. The dump contains data for cities and events. 
   Command: mysql -u USER_NAME -p DB_NAME < aertrip.sql
```

## Execution

```python
1.  Open the terminal and launch the application. Command: python extract_entities.py
    
2.  Open any web browser and enter the URL: '0.0.0.0:8500/parse_query?q=<YOUR QUERY>'.
    
```

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

## License
[Syed Asnal Rizvi](sarizvi@zhcet.ac.in)
